"use strict"

let tabsContainer = document.querySelector('.container-tabs');
let contentContainer = document.querySelector('.container-content');



tabsContainer.addEventListener('click', (e) => {
tabsContainer.querySelector('.tabs-active')?.classList.toggle('tabs-active')
let target = e.target;

if(e.target.closest('li')){
   e.target.classList.toggle('tabs-active')
}
const currentId = target.dataset.forTab;
console.log(currentId);
contentContainer.querySelector('.content-active')?.classList.toggle('content-active');
 contentContainer.querySelector(`.content[data-tab="${currentId}"]`)?.classList.toggle("content-active");
});
